#include "BaseCharacter.h"

BaseCharacter::BaseCharacter(Vector2 windowDimensions, Texture2D idleTexture, Texture2D runTexture) 
{
    // Set character size in screen
    width = texture.width / AnimData.maxFrame;
    height = texture.height;
    
    texture = idleTexture;
    idle = idleTexture;
    run = runTexture;
}

void BaseCharacter::tick(float deltaTime)
{
    /** HANDLING USER INPUT */
    direction = GetCharacterDirection();
    //printf("%f  ", direction.x);

    /** PERFORM MOVEMENT */
    if (Vector2Length(direction) != 0.0)
    {
        lastFrameWorldPos = worldPos;
        worldPos = Vector2Add(worldPos, Vector2Scale(Vector2Normalize(direction), speed));
        leftRight = direction.x < 0.f ? -1.f : 1.f;

        texture = run;
    }
    else
        texture = idle;

    
    UpdateAnimation(deltaTime);

    /** DRAW CHARACTER */
    // Create source rectangle
    Rectangle source { width * AnimData.frame, 0.f, leftRight * width, height };

    // Create destination rectangle
    Rectangle dest { GetDestRectangle() };

    // Draw the character into the scene
    DrawTexturePro(
        Vector2Length(direction) > 0 ? run : idle, // Switch between idle and running states
        source,
        dest,
        Vector2{},
        0,
        WHITE
    );
}

/** FUNCTION TO OVERRIDE */
Vector2 BaseCharacter::GetCharacterDirection() 
{
    return direction;
}

void BaseCharacter::UpdateAnimation(float DeltaTime)
{
    /** UPDATE CHARACTER ANIMATION
     * Add DeltaTime to current running time
     * If running time is greater than the time between frames, update frame
     *  * Then reset running time to 0
     *  * Then, if current frame is greater than the last anim frame, restart animation
     */
    AnimData.runningTime += DeltaTime;
    if (AnimData.runningTime >= AnimData.updateTime)
    {
        AnimData.frame++;
        AnimData.runningTime = 0.f;

        if (AnimData.frame > AnimData.maxFrame)
            AnimData.frame = 0;
    }
}

/** FUNCTION TO OVERRIDE */
Rectangle BaseCharacter::GetDestRectangle()
{
    return Rectangle();
}

/** GO BACK ONE FRAME */
void BaseCharacter::RevertWorldPos () 
{
    worldPos = lastFrameWorldPos;
}

/** GET THE COLLISION AS A RECTANGLE */
Rectangle BaseCharacter::GetCollisionRect()
{
    return Rectangle{
        getScreenPos().x,
        getScreenPos().y,
        width * scale,
        height * scale
    };
}
