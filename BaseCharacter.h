#ifndef BASE_CHARACTER_H
#define BASE_CHARACTER_H

#include "raylib.h"
#include "raymath.h"
#include <iostream>


struct AnimData
{
    float runningTime;
    const float updateTime;
    int frame;
    const int maxFrame;
};

class BaseCharacter
{
public:
    // Basic functions
    BaseCharacter (Vector2 windowDimensions, Texture2D idleTexture, Texture2D runTexture);
    virtual void tick(float deltaTime);

    // Position functions
    Vector2 GetWorldPos() { return worldPos; }
    virtual Vector2 getScreenPos() = 0;
    void RevertWorldPos();

    // Collision
    Rectangle GetCollisionRect();

    // Living stuff
    bool GetAlive() { return alive; }
    void SetAlive(bool isAlive) { alive = isAlive; }

protected:
    // Texture stuff
    Texture2D texture {LoadTexture("characters/knight_idle_spritesheet.png")};
    Texture2D idle {LoadTexture("characters/knight_idle_spritesheet.png")};
    Texture2D run {LoadTexture("characters/knight_idle_spritesheet.png")};
    float width {};
    float height {};
    float scale { 4.f };

    // Movement
    Vector2 worldPos {};
    Vector2 lastFrameWorldPos {};
    Vector2 direction {};
    float leftRight { 1.f };
    float speed { 4.0f };
    AnimData AnimData { 0, 1.f / 12.f, 0, 6 };

    // Misc
    bool alive { true };

    // Functions
    virtual Vector2 GetCharacterDirection();
    virtual void UpdateAnimation(float DeltaTime);
    virtual Rectangle GetDestRectangle();
};


#endif