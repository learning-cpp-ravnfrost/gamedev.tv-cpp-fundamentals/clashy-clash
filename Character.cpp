#include "Character.h"

Character::Character(Vector2 winDimensions, Texture2D idleTex, Texture2D runTex) : BaseCharacter(windowDimensions, idleTex, runTex)
{
    // Set character position in screen
    windowDimensions = winDimensions;
}

void Character::tick(float deltaTime)
{
    // There's no point in trying to tick if character is dead
    if(!GetAlive()) return;

    // If it's not dead, tick parent first
    BaseCharacter::tick(deltaTime);

    // Register player input for attack
    SetAttacking(IsKeyDown(KEY_SPACE));

    // Sword parameters
    Vector2 origin {};
    Vector2 offset {};
    float swordRotation {};

    // Flip sword depending if character is moving left or right
    if(leftRight > 0.f) 
    {
        origin = { 0.f, weapon.height * scale };
        offset = { 35.f, 55.f };
        weaponColliderRec = {
            getScreenPos().x + offset.x,
            getScreenPos().y + offset.y - weapon.height * scale,
            weapon.height * scale,
            weapon.width * scale
        };
        swordRotation = IsAttacking() ? 35.f : 0.f;
         
        }
    else
    {
        origin = { weapon.width * scale, weapon.height * scale };
        offset = { 25.f, 55.f };
        weaponColliderRec = {
            getScreenPos().x + offset.x - weapon.width * scale,
            getScreenPos().y + offset.y - weapon.height * scale,
            weapon.height * scale,
            weapon.width * scale
        };
        swordRotation = IsAttacking() ? -35.f : 0.f;
    }

    // Perform the drawing of the sword
    Rectangle source {0.f, 0.f, static_cast<float>(weapon.width) * leftRight, static_cast<float>(weapon.height)};
    Rectangle dest {getScreenPos().x + offset.x, getScreenPos().y + offset.y, weapon.width * scale, weapon.height * scale};
    DrawTexturePro(weapon, source, dest, origin, swordRotation, WHITE);
}

Vector2 Character::getScreenPos()
{
    return {
        windowDimensions.x / 2.f - scale * (.5f * width),
        windowDimensions.y / 2.f - scale * (.5f * height)
    };
}

/** MOVE CHARACTER ACCORDING TO KEYS PRESSED */
Vector2 Character::GetCharacterDirection() {
    Vector2 localDirection {};
    if (IsKeyDown(KEY_A))
        localDirection.x -= 1.0;
    if (IsKeyDown(KEY_D))
        localDirection.x += 1.0;
    if (IsKeyDown(KEY_W))
        localDirection.y -= 1.0;
    if (IsKeyDown(KEY_S))
        localDirection.y += 1.0;

    return localDirection;
}

Rectangle Character::GetDestRectangle()
{
    Rectangle dest { getScreenPos().x, getScreenPos().y, scale * width, scale * height };
    return dest;
}

/** RECEIVE DAMAGE AND DIE IF HEALTH GOES DOWN TO 0 */
void Character::takeDamage(float damage)
{
    health -= damage;

    if (health <= 0) SetAlive(false);
}
