#ifndef CHARACTER_H
#define CHARACTER_H

#include "BaseCharacter.h"

class Character : public BaseCharacter {

public:
    // Basic function overrides
    Character (Vector2 windowDimensions, Texture2D idleTex, Texture2D runTex);
    void tick(float deltaTime);

    // Override screen pos function
    Vector2 getScreenPos() override;

    // Combat attack public functions
    Rectangle GetWeaponCollider() const { return weaponColliderRec; }
    bool IsAttacking() const { return isAttacking; }
    void SetAttacking(bool attacking) { isAttacking = attacking; }

    // Combat health public functions
    float getHealth() const { return health; }
    void takeDamage(float damage);

private:
    // Visual stuff
    Vector2 windowDimensions {};
    Texture2D weapon { LoadTexture("characters/weapon_sword.png") };

    // Combat stuff
    Rectangle weaponColliderRec {};
    bool isAttacking { false };
    float health { 100.f };

    // Functions
    Vector2 GetCharacterDirection() override;
    Rectangle GetDestRectangle() override;
};

#endif