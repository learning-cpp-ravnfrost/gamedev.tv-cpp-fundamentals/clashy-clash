#include "Enemy.h"

Enemy::Enemy(Vector2 windowDimensions, Texture2D idleTex, Texture2D runTex, Vector2 pos, Character* knightChar) 
: BaseCharacter(windowDimensions, idleTex, runTex)
{
    worldPos = pos;
    knight = knightChar;
    speed = 2.f;
}

void Enemy::tick(float deltaTime)
{
    if(!GetAlive()) return;
    BaseCharacter::tick(deltaTime);

    // Damage player per tick (dps) on contact
    if ( CheckCollisionRecs(knight -> GetCollisionRect(), GetCollisionRect())) 
    {
        knight -> takeDamage(damagePerSec * deltaTime);
    }
}

/** GET POSITION BASED ON KNIGHT POSITION */
Vector2 Enemy::getScreenPos()
{
    return Vector2Subtract(worldPos, knight->GetWorldPos());
}

/** GET THE MOVEMENT DIRECTION CALCULATED WITH SELF POS AND KNIGHT POS */
Vector2 Enemy::GetCharacterDirection() 
{
    direction = Vector2Distance(getScreenPos(), knight -> getScreenPos()) > 20.f ? 
        Vector2Normalize({Vector2Subtract(knight->getScreenPos(), getScreenPos())}) :
        Vector2 {0.f, 0.f};
    return direction;
}

/** GET DRAWING RECTANGLE */
Rectangle Enemy::GetDestRectangle()
{
    Rectangle dest { getScreenPos().x, getScreenPos().y, scale * width, scale * height };
    return dest;
}
