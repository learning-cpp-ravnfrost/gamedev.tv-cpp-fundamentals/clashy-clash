#include "BaseCharacter.h"
#include "Character.h"

class Enemy : public BaseCharacter {

public:
    Enemy (Vector2 windowDimensions, Texture2D idleTex, Texture2D runTex, Vector2 pos, Character* knight);
    void tick(float deltaTime);
    Vector2 getScreenPos() override;

private:
    Vector2 GetCharacterDirection() override;
    Character* knight;
    Rectangle GetDestRectangle() override;

    float damagePerSec { 10.f };
};