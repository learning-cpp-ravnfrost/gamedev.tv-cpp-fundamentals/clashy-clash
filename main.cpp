#include "raylib.h"
#include "raymath.h"
#include <iostream>
#include "Character.h"
#include "prop.h"
#include "Enemy.h"
#include <string.h>

float GetRandomCoord() { return 100 + ( std::rand() % ( 1600 - 100 + 1 ) ); }

int main()
{
    /** WINDOW SETUP */
    const Vector2 windowDimensions{384, 384};
    InitWindow(windowDimensions.x, windowDimensions.y, "Top-down raylib gamedev.tv project");
    SetTargetFPS(60);

    /** SCENE SETUP */
    const Texture2D MapTexture{LoadTexture("nature_tileset/map2.png")};
    Vector2 mapPos{0, 0};
    float mapScale {3.f};

    /** PLAYER SETUP */
    Character knight { 
        windowDimensions, 
        LoadTexture("characters/knight_idle_spritesheet.png"), 
        LoadTexture("characters/knight_run_spritesheet.png") 
    };

    /** PROPS SETUP */
    Prop props[6] {
        { Vector2{150, 300}, LoadTexture("nature_tileset/Rock.png"), 4.f },
        { Vector2{430, 600}, LoadTexture("nature_tileset/Log.png"), 4.f },
        { Vector2{1000, 320}, LoadTexture("nature_tileset/Bush.png"), 4.f },
        { Vector2{750, 450}, LoadTexture("nature_tileset/Rock.png"), 4.f },
        { Vector2{800, 1200}, LoadTexture("nature_tileset/Log.png"), 4.f },
        { Vector2{1600, 1850}, LoadTexture("nature_tileset/Sign.png"), 4.f }
    };

    Enemy goblin { 
        windowDimensions, 
        LoadTexture("characters/goblin_idle_spritesheet.png"), 
        LoadTexture("characters/goblin_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight
    };
       
    Enemy slime {
        windowDimensions,
        LoadTexture("characters/slime_idle_spritesheet.png"), 
        LoadTexture("characters/slime_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };
    Enemy goblin2 {
        windowDimensions,
        LoadTexture("characters/goblin_idle_spritesheet.png"), 
        LoadTexture("characters/goblin_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };
       
    Enemy slime2 {
        windowDimensions,
        LoadTexture("characters/slime_idle_spritesheet.png"), 
        LoadTexture("characters/slime_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };
    Enemy goblin3 {
        windowDimensions,
        LoadTexture("characters/goblin_idle_spritesheet.png"), 
        LoadTexture("characters/goblin_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };
       
    Enemy slime3 {
        windowDimensions,
        LoadTexture("characters/slime_idle_spritesheet.png"), 
        LoadTexture("characters/slime_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };
    Enemy goblin4 {
        windowDimensions,
        LoadTexture("characters/goblin_idle_spritesheet.png"), 
        LoadTexture("characters/goblin_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };
       
    Enemy slime4 {
        windowDimensions,
        LoadTexture("characters/slime_idle_spritesheet.png"), 
        LoadTexture("characters/slime_run_spritesheet.png"), 
        {GetRandomCoord(), GetRandomCoord()},
        &knight  
    };

    Enemy* enemies [8] {
        &goblin,
        &slime,
        &goblin2,
        &slime2,
        &goblin3,
        &slime3,
        &goblin4,
        &slime4,
    };
    

    while (!WindowShouldClose())
    {
        BeginDrawing();
        ClearBackground(BLUE);

        mapPos = Vector2Scale(knight.GetWorldPos(), -1.f);

        /** DRAW STUFF */
        // Map
        DrawTextureEx(MapTexture, mapPos, 0, mapScale, WHITE);

        /** DRAW THE PROPS */
        for (auto prop : props) 
        {
            prop.Render(knight.GetWorldPos());
        }

        /** DISPLAY TEXT ON THE SCREEN
         * If player is unalive, display Game Over and stop rendering further
         * If player is alive, display health
         */
        if(!knight.GetAlive()) 
        {
            DrawText("Game Over!", 55.f, 45.f, 40, RED);
            EndDrawing();
            continue;
        }
        else
        {
            std::string healthDisplay = "Health: ";
            healthDisplay.append(std::to_string(knight.getHealth()), 0, 5);
            DrawText(healthDisplay.c_str(), 10.f, 10.f, 30, RED);
        }

        

        /** PERFORM CHARACTERS TICK */
        knight.tick(GetFrameTime());
        for(auto enemy : enemies) 
        {
            enemy -> tick(GetFrameTime());
        }

        /** KILL GOBLINS WHEN ATTACKED BY PLAYER */
        if ( knight.IsAttacking() ) 
            for(auto enemy : enemies) 
                if ( CheckCollisionRecs(enemy -> GetCollisionRect(), knight.GetWeaponCollider()) )
                    enemy -> SetAlive(false);

        /** DISALLOW PLAYER FROM EXITING BOUNDS */
        if (
            knight.GetWorldPos().x < 0.f ||
            knight.GetWorldPos().y < 0.f ||
            knight.GetWorldPos().x + windowDimensions.x > MapTexture.width * mapScale ||
            knight.GetWorldPos().y + windowDimensions.y > MapTexture.height * mapScale
        )
        {
            knight.RevertWorldPos();
        }

        /** DISALLOW PLAYERS FROM CLIPPING INTO PROPS */
        for(auto prop : props) {
            if( CheckCollisionRecs(prop.getCollisionRec(knight.GetWorldPos()), knight.GetCollisionRect()) )
                knight.RevertWorldPos();
        }

        EndDrawing();
    }

    /** UNLOAD TEXTURES*/
    UnloadTexture(MapTexture);

    CloseWindow();
}
