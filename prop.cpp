#include "prop.h"

Prop::Prop(Vector2 pos, Texture2D tex, float scal) : 
    worldPos(pos),
    texture(tex),
    scale(scal)
{

}

void Prop::Render(Vector2 knightpos)
{
    Vector2 screenPos = Vector2Subtract(worldPos, knightpos);

    DrawTextureEx(texture, screenPos, 0, scale, WHITE);
}

Rectangle Prop::getCollisionRec(Vector2 knightpos)
{
    Vector2 screenPos = Vector2Subtract(worldPos, knightpos);
    return Rectangle{
        screenPos.x,
        screenPos.y,
        texture.width * scale,
        texture.height * scale
    };
}
