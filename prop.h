#include "raylib.h"
#include "raymath.h"

class Prop {
public:
    Prop(Vector2 pos, Texture2D tex, float scal);

private:
    Vector2 worldPos {};
    Texture2D texture {};
    float scale;

public:
    void Render(Vector2 knightpos);
    Rectangle getCollisionRec(Vector2 knightpos);
};